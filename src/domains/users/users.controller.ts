import { Body, Controller, Get, HttpException, Post, Put, Request, UseFilters, UseGuards, UsePipes } from '@nestjs/common'
import { ConfigService } from 'src/configs/configs.service'
import { AccessToken } from './models/user.interface'
import { CreateOneUserDto, LoginDto, UpdateOneUserDto, UpdatePasswordDto } from './models/users.dto'
import { UsersService } from './users.service'
import * as bcrypt from 'bcrypt'
import { AuthService } from '../../middleware/auth/auth.service'
import { User } from './models/users.schema'
import { AuthGuard } from '@nestjs/passport'
import { ValidationPipe } from '../../middleware/pipes/validation.pipe'
import { HttpExceptionFilter } from '../../middleware/exceptions/exception'
const configService = new ConfigService()

@Controller()
@UseFilters(new HttpExceptionFilter())
export class UsersController {
  constructor(private readonly usersService: UsersService,
              private readonly authService: AuthService) { }

  @Post('auth/register')
  @UsePipes(new ValidationPipe())
  async registerUser(@Body() createUserDto: CreateOneUserDto): Promise<User> {
    try {
      const { userName, password } = createUserDto

      const [isExistUser, hashedPassword ] = await Promise.all([
        await this.usersService.checkUserExist(userName),
        await bcrypt.hash(password, configService.bcryptData.saltRounds),
      ])

      if (isExistUser) {
        throw {
          code: 400,
          name: 'UserNameAlreadyExist',
        }
      }

      const userData = {
        ...createUserDto,
        password: hashedPassword,
        lastUpdatePassword: new Date(),
      }

      const user = await this.usersService.createOne(userData)

      user.password = undefined

      return user

    } catch (error) {
      throw error
    }
  }

  @Post('auth/login')
  @UsePipes(new ValidationPipe())
  async login(@Body() loginDto: LoginDto): Promise<AccessToken> {
    try {
      const { userName, password } = loginDto
      const query = {
        userName,
      }
      const user = await this.usersService.findOne(query)

      if (!user) {
        throw {
          name: 'UserNotFound',
          code: 404,
        }
      }

      const checkPassword = await bcrypt.compare(password, user.password)

      if (!checkPassword) {
        throw {
          name: 'InvalidPassword',
          code: 400,
        }
      }

      const { accessToken } = await this.authService.generateJWT(user)

      return {
        userID: user._id,
        accessToken,
      }
    } catch (error) {
      throw error
    }
  }

  @Get('users/me')
  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  async getUserInfo(
    @Request() { user }
  ): Promise<User> {
    try {
      user.password = undefined
      return user
    } catch (error) {
      throw error
    }
  }

  @Put('users/me')
  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  async updateMe(
    @Request() { user },
    @Body() updateMeDto: UpdateOneUserDto
  ): Promise<boolean> {
    try {
      await this.usersService.updateOne({
        updateData: updateMeDto,
        userID: user._id,
      })

      return true
    } catch (error) {
      throw error
    }
  }

  @Put('users/me/password')
  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  async updatePassword(
    @Request() { user },
    @Body() updatePasswordDto: UpdatePasswordDto
    ): Promise<AccessToken> {
    try {
      const { oldPassword, newPassword, confirmPassword } = updatePasswordDto
      const { password: originPassword } = await this.usersService.findOne({ _id: user._id })

      const [ checkPassword, compareOldPassword, compareNewPassword ] = await Promise.all([
        await bcrypt.compare(oldPassword, originPassword),
        await bcrypt.compare(newPassword, originPassword),
        confirmPassword === newPassword,
      ])

      if (!checkPassword) {
        throw {
          name: 'Nhap sai mat khau ban oi, doi the deo nao duoc!',
          code: 400,
        }
      }

      if (compareOldPassword) {
        throw {
          name: 'Nhap mat khau cu a`?, ?? :D ??',
          code: 400
        }
      }

      if (!compareNewPassword) {
        throw {
          name: 'Xac nhan mat khau sau ban oi',
          code: 400
        }
      }

      const hashedPassword = await bcrypt.hash(newPassword, configService.bcryptData.saltRounds)
      const updatedUser = await this.usersService.updateOne({
        userID: user._id,
        updateData: {
          password: hashedPassword,
          lastUpdatePassword: new Date()
        }
      })

      const { accessToken } = await this.authService.generateJWT(updatedUser)

      return {
        userID: updatedUser._id,
        accessToken,
      }
    } catch (error) {
      throw error
    }
  }
}