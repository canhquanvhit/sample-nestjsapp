
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'
import { Role } from '../../roles/models/roles.schema'
import * as mongoose from 'mongoose'

export type UserDocument = User & Document

@Schema()
export class User {

  @Prop({ required: true , unique: true})
  userName: string

  @Prop({ required: true })
  password: string

  @Prop({ required: true })
  phoneNumber: string

  @Prop({ required: true })
  lastUpdatePassword: Date

  @Prop()
  avatar: string

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Role', required: true})
  role: Role
}

export const usersSchema = SchemaFactory.createForClass(User)
usersSchema.set('timestamps', true)