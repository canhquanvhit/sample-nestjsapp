import { RoleName } from 'src/domains/roles/models/roles.schema'

export interface User {
  id?: string,
  userName: string,
  email: string,
  phoneNumber: string,
}

export interface UserInfo {
  _id: string,
  userName: string,
  phoneNumber: string,
  lastUpdatePassword: Date,
}

export interface AccessToken {
  accessToken: string,
  userID: string,
}

export interface CreateOneUserInterface {
  userName: string,
  password: string,
  phoneNumber: string,
  role: string
}

export interface UpdateOneUser {
  userName?: string,
  phoneNumber?: string,
  password?: string,
  lastUpdatePassword?: Date
}

export interface UpdateOneService {
  userID: string,
  updateData: UpdateOneUser,
}

export interface FindOneInterface {
  _id?: string,
  userName?: string
}