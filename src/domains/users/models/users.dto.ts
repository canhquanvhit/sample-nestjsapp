import { IsMongoId, isNotEmpty, IsNotEmpty, IsNumber, IsOptional, IsString, Matches } from 'class-validator'
import { RoleName } from '../../roles/models/roles.schema'

export class CreateOneUserDto {
  @IsString()
  @IsMongoId()
  @IsOptional()
  readonly _id?: string

  @IsString()
  @IsNotEmpty()
  readonly userName: string

  @IsString()
  @IsNotEmpty()
  readonly password: string

  @Matches(/(\+84|84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  @IsNotEmpty()
  readonly phoneNumber: string

  @IsString()
  @IsOptional()
  readonly avatar?: string

  @IsString()
  @IsNotEmpty()
  readonly role: string
}

export class LoginDto {
  @IsString()
  @IsNotEmpty()
  readonly userName: string

  @IsString()
  @IsNotEmpty()
  readonly password: string
}

export class UpdateOneUserDto {
  @IsString()
  @IsOptional()
  readonly userName?: string

  @Matches(/(\+84|84|0[3|5|7|8|9])+([0-9]{8})\b/g)
  @IsOptional()
  readonly phoneNumber?: string

  @IsString()
  @IsOptional()
  readonly avatar?: string
}

export class UpdatePasswordDto {
  @IsString()
  @IsNotEmpty()
  readonly oldPassword: string

  @IsString()
  @IsNotEmpty()
  readonly newPassword: string

  @IsString()
  @IsNotEmpty()
  readonly confirmPassword: string
}