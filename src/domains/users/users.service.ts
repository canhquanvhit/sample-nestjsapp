import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/mongoose'
import { Model } from 'mongoose'
import { User, UserDocument } from './models/users.schema'
import { CreateOneUserInterface, FindOneInterface, UpdateOneService } from './models/user.interface'
@Injectable()
export class UsersService {
  constructor(@InjectModel(User.name) private readonly usersModel: Model<UserDocument>) { }


  async checkUserExist(userName: string): Promise<boolean> {
    const user = await this.usersModel.findOne({ userName })

    if (!user) {
      return false
    }

    return true
  }

  async findOne(query: FindOneInterface): Promise<UserDocument | undefined> {
    const user = await this.usersModel.findOne(query)
      .populate({
        path: 'role'
      })
      .exec()

    if (!user) {
      return null
    }

    return user
  }

  async createOne(userData: CreateOneUserInterface): Promise<UserDocument> {
    try {
      const user = new this.usersModel(userData)
      await user.save()

      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async findMany(): Promise<User[]> {
    const users = await this.usersModel.find()

    return users
  }

  async updateOne({ userID, updateData}: UpdateOneService): Promise<UserDocument> {
    try {
      const user = await this.usersModel.findOne({ _id: userID})

      if (!user) {
        return Promise.reject({
          code: 404,
          name: 'UserNotFound'
        })
      }

      for (const key in updateData) {
        if (updateData[key]) {
          console.log(user[key])
          user[key] = updateData[key]
          console.log(user[key])
        }
      }
      user.save()

      return user
    } catch (error) {
      return Promise.reject(error)
    }
  }
}

