
import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose'
import { Document } from 'mongoose'

export type RoleDocument = Role & Document
export enum RoleName {
  admin = 'ADMIN',
  client = 'CLIENT',
}
@Schema()
export class Role {

  @Prop({ required: true , unique: true})
  roleName: RoleName

}

export const rolesSchema = SchemaFactory.createForClass(Role)
rolesSchema.set('timestamps', true)