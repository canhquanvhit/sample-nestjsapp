import { IsEnum, IsNotEmpty, IsString } from 'class-validator'
export const Name = [
  'CLIENT',
  'ADMIN'
]

export class CreateRoleDto {
  @IsEnum(Name)
  @IsString()
  @IsNotEmpty()
  readonly roleName: string

}