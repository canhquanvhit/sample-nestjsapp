import { forwardRef, Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { AuthModule } from '../../middleware/auth/auth.module'
import { Role, rolesSchema } from './models/roles.schema'
import { RolesController } from './roles.controller'
import { RolesService } from './roles.service'


@Module({
  imports: [
    MongooseModule.forFeature([{ name: Role.name, schema: rolesSchema}]),
    forwardRef(() => AuthModule)
  ],
  controllers: [RolesController],
  providers: [RolesService],
  exports: [RolesService],
})
export class RolesModule { }