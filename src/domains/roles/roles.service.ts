import { Injectable } from '@nestjs/common'
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'
import { Role, RoleDocument, RoleName } from './models/roles.schema'
import { generateObjectId } from '../../helpers/get'
import { roles } from 'src/constants/roles'
import { CreateOneInterface } from './models/roles.interface'

@Injectable()
export class RolesService {
  constructor(@InjectModel(Role.name) private readonly roleModel: Model<RoleDocument>) { }

  private roles = [
    {
      _id: generateObjectId(),
      roleName: RoleName.admin,
      scopes: [],
    },
    {
      _id: generateObjectId(),
      roleName: RoleName.client,
      scopes: roles.user,
    }
  ]
  async findMany(): Promise<Role[]> {
    return await this.roleModel.find()
  }

  async findOne(query): Promise<Role> {
    try {
      const item = await this.roleModel.findOne(query)

      if (!item) {
        return Promise.reject({
          name: 'ItemNotFound',
          code: '404',
        })
      }

      return item
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async createOne(createOneData: CreateOneInterface): Promise<Role> {
    try {
      const role = new this.roleModel(createOneData)
      await role.save()

      return role
    } catch (error) {
      return Promise.reject(error)
    }
  }

}
