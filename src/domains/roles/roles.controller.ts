import { Body, Controller, Get, Post, UseFilters, UsePipes, ValidationPipe } from '@nestjs/common'
import { HttpExceptionFilter } from '../../middleware/exceptions/exception'
import { CreateRoleDto } from './models/roles.dto'
import { Role } from './models/roles.schema'
import { RolesService } from './roles.service'

@Controller('roles')
export class RolesController {
  constructor(
    private readonly rolesService: RolesService,
  ) { }

  @Post()
  @UseFilters(new HttpExceptionFilter())
  @UsePipes(new ValidationPipe())
  async createRole (@Body() createRoleDto: CreateRoleDto): Promise<Role> {
    try {
      const error = new Error('abc')
      throw error
      // const role = await this.rolesService.createOne(createRoleDto)
      // return role
    } catch (error) {
      return Promise.reject(error)
    }
  }

  @Get()
  @UsePipes(new ValidationPipe())
  async getAll (): Promise<Role[]> {
    try {
      const roles = await this.rolesService.findMany()
      return roles
    } catch (error) {
      throw error
    }
  }


}