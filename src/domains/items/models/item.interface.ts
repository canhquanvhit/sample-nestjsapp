import { Document } from 'mongoose'

export interface Item {
    id?: string,
    name: string,
    description: string,
    quantity: number,
}

export interface CreateOneInterface {
    name: string,
    description?: string,
    quantity: number,
}

export interface UpdateOneItem {
    name?: string,
    description?: string,
    quantity?: number,
}

export interface UpdateOneService {
    id: string,
    updateOneInterface: UpdateOneItem
}