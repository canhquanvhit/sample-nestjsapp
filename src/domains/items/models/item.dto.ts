import { IsMongoId, IsNotEmpty, IsNumber, IsOptional, IsString } from 'class-validator'

export class CreateItemDto {
    @IsString()
    @IsMongoId()
    @IsOptional()
    readonly _id?: string

    @IsString()
    @IsNotEmpty()
    readonly name: string

    @IsString()
    @IsOptional()
    @IsOptional()
    readonly description?: string

    @IsNumber()
    @IsNotEmpty()
    readonly quantity: number
}

export class UpdateItemDto {
    @IsString()
    @IsOptional()
    readonly name?: string

    @IsString()
    @IsOptional()
    @IsOptional()
    readonly description?: string

    @IsNumber()
    @IsOptional()
    readonly quantity?: number
}