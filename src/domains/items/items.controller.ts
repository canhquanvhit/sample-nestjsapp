import { Body, Controller, Delete, Get, Param, Post, Put, UseFilters, UseGuards, UsePipes } from '@nestjs/common'
import { AuthGuard } from '@nestjs/passport'
import { HttpExceptionFilter } from '../../middleware/exceptions/exception'
import { scopes } from '../../constants/scopes'
import { Scopes } from '../../middleware/authz/authz.service'
import { ValidationPipe } from '../../middleware/pipes/validation.pipe'
import { ItemsService } from './items.service'
import { CreateItemDto, UpdateItemDto } from './models/item.dto'
import { Item } from './models/item.interface'

@Controller('items')
@UseFilters(new HttpExceptionFilter())
export class ItemsController {
  constructor(private readonly itemsService: ItemsService) { }

  @Get()
  @UseGuards(new Scopes([
    scopes.USER_USER_READ,
  ]))
  @UseGuards(AuthGuard('jwt'))
  async findAll(): Promise<Item[]> {
    return await this.itemsService.findMany()
  }

  @Get('/:id')
  @UseGuards(new Scopes([
    scopes.USER_USER_READ,
  ]))
  @UseGuards(AuthGuard('jwt'))
  async findOne(@Param() { id }): Promise<Item> {
    try {
      return await this.itemsService.findOne(id)
    } catch (error) {
      throw error
    }
  }

  @Post()
  @UseGuards(new Scopes([
    scopes.USER_USER_CREATE,
  ]))
  @UseGuards(AuthGuard('jwt'))
  @UsePipes(new ValidationPipe())
  async createOne(@Body() createItemDto: CreateItemDto): Promise<Item> {
    try {
      const item = await this.itemsService.createOne(createItemDto)

      return item
    } catch (error) {
      throw error
    }
  }

  @Put('/:id')
  @UseGuards(new Scopes([
    scopes.USER_USER_UPDATE,
  ]))
  @UsePipes(new ValidationPipe())
  @UseGuards(AuthGuard('jwt'))
  async updateOne(
      @Param() { id },
      @Body() updateOneItemDto: UpdateItemDto): Promise<boolean> {
    try {
      const result = await this.itemsService.updateOne({
        id,
        updateOneInterface: updateOneItemDto
      })

      return result
    } catch (error) {
      throw error
    }
  }

  @Delete('/:id')
  @UseGuards(new Scopes([
    scopes.USER_USER_DELETE,
  ]))
  @UseGuards(AuthGuard('jwt'))
  async deleteOne(@Param() { id }): Promise<boolean> {
    try {
       return await this.itemsService.deleteOne(id)
    } catch (error) {
      throw error
    }
  }
}
