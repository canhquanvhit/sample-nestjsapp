import { Injectable } from '@nestjs/common'
import { Model } from 'mongoose'
import { InjectModel } from '@nestjs/mongoose'
import { CreateOneInterface, UpdateOneService } from './models/item.interface'
import { Item, ItemDocument } from './models/items.schema'
@Injectable()
export class ItemsService {
  constructor(@InjectModel(Item.name) private readonly itemModel: Model<ItemDocument>) { }

  async findMany(): Promise<Item[]> {
    return await this.itemModel.find()
  }

  async findOne(id): Promise<Item> {
    try {
      const item = await this.itemModel.findOne({ _id: id})

      if (!item) {
        return Promise.reject({
          name: 'ItemNotFound',
          code: '404',
        })
      }

      return item
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async createOne(itemData: CreateOneInterface): Promise<Item> {
    try {
      const item = new this.itemModel(itemData)
      await item.save()

      return item

    } catch (error) {
      return Promise.reject(error)
    }
  }

  async deleteOne(id): Promise<boolean> {
    try {
      const item = await this.itemModel.findOne({ _id: id})

      if (!item) {
        return Promise.reject({
          name: 'ItemNotFound',
          code: 404,
        })
      }

      await this.itemModel.remove(item)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }

  async updateOne({ id, updateOneInterface}: UpdateOneService): Promise<boolean> {
    try {
      const item: any = await this.itemModel.findOne({ _id: id})

      if (!item) {
        return Promise.reject({
          name: 'ItemNotFound',
          code: 404,
        })
      }

      await this.itemModel.updateOne(item, updateOneInterface)

      return true
    } catch (error) {
      return Promise.reject(error)
    }
  }
}
