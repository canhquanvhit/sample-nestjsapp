import { config, parse } from 'dotenv'
import { readFileSync } from 'fs'

config()
export interface EnvConfig {
    [key: string]: string
}

export class ConfigService {

    private configEnv

    private bcrypt = {
        saltRounds: 10,
        myPlaintextPassword: 's0/\/\P4$$w0rD',
        someOtherPlaintextPassword: 'not_bacon',
    }

    private jwt = {
        jwtSecret: 'local3@.12#!F',
    }

    constructor() {
        const filePath = 'local.env'

        const configEnv = parse(readFileSync(filePath))
        this.configEnv = configEnv
    }

    get dbConfig () {
        return {
            dbHost: this.configEnv.DB_HOST,
            dbName: this.configEnv.DB_NAME,
            dbUser: this.configEnv.DB_USER,
            dbPass: this.configEnv.DB_PASS
        }
    }

    get mongoURI () {
        return `mongodb+srv://${this.dbConfig.dbUser}:${this.dbConfig.dbPass}@${this.dbConfig.dbHost}/${this.dbConfig.dbName}?retryWrites=true&w=majority`
    }

    get bcryptData () {
        return this.bcrypt
    }

    get jwtSecret () {
        return this.jwt.jwtSecret
    }
}