import { scopes } from './scopes'

export const roles = {
  user: [
    scopes.USER_USER_READ,

    scopes.USER_ITEM_READ,
  ]
}