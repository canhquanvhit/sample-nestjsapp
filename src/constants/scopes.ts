export enum scopes {
  // user
  USER_USER_CREATE = 'user:user:create',
  USER_USER_UPDATE = 'user:user:update',
  USER_USER_DELETE = 'user:user:delete',
  USER_USER_READ = 'user:user:read',

  USER_ITEM_READ = 'user:item:read',
  USER_ITEM_UPDATE = 'user:item:update',
  USER_ITEM_DELETE = 'user:item:delete',
  USER_ITEM_CREAT = 'user:item:create',
}