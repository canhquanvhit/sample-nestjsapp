import { Module } from '@nestjs/common'
import { MongooseModule } from '@nestjs/mongoose'
import { ItemModule } from './domains/items/items.module'
import { ConfigService } from './configs/configs.service'
import { AuthModule } from './middleware/auth/auth.module'
import { UsersModule } from './domains/users/user.module'
import { RolesModule } from './domains/roles/roles.module'
import { APP_FILTER } from '@nestjs/core'
import { HttpExceptionFilter } from './middleware/exceptions/exception'
const configService = new ConfigService()
@Module({
  imports: [
    RolesModule,
    ItemModule,
    UsersModule,
    AuthModule,
    MongooseModule.forRoot(configService.mongoURI), AuthModule],
  providers: [
    {
      provide: APP_FILTER,
      useClass: HttpExceptionFilter,
    }
  ]
})
export class AppModule { }
