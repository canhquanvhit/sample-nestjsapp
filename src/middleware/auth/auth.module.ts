import { Module } from '@nestjs/common'
import { AuthService } from './auth.service'
import { UsersModule } from '../../domains/users/user.module'
import { ConfigModule } from 'src/configs/configs.module'
import { PassportModule } from '@nestjs/passport'
import { JwtStrategy } from './jwt.strategy'

@Module({
  imports: [
    UsersModule,
    ConfigModule,
    PassportModule,
  ],
  providers: [
    AuthService,
    JwtStrategy,
  ],
  exports: [ AuthService ],
})
export class AuthModule { }
