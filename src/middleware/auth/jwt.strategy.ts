import { ExtractJwt, Strategy } from 'passport-jwt'
import { PassportStrategy } from '@nestjs/passport'
import { Injectable, UnauthorizedException } from '@nestjs/common'
import { ConfigService } from 'src/configs/configs.service'
import { UsersService } from 'src/domains/users/users.service'

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(
    configService: ConfigService,
    private readonly usersService: UsersService,
  ) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: configService.jwtSecret
    })
  }

  async validate(payload: any) {
    const { userID, changePasswordAt } = payload

    const query = {
      _id: userID,
    }
    const user = await this.usersService.findOne(query)

    if (user?.lastUpdatePassword.toISOString() !== changePasswordAt) {
      throw new UnauthorizedException()
    }
    return user
  }
}