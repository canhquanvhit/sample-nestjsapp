import { Injectable } from '@nestjs/common'
import { UsersService } from '../../domains/users/users.service'
import { AccessToken, TokenPayload } from './models/auth.interface'
import * as jsonwebtoken from 'jsonwebtoken'
import { ConfigService } from '../../configs/configs.service'
import * as bcrypt from 'bcrypt'
@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly configService: ConfigService
  ) { }

  async validateUser(userName: string, pass: string): Promise<any> {
    const query = {
      userName,
    }
    const user = await this.usersService.findOne(query)

    const match = await bcrypt.compare(pass, user.password)
    if (!match) {
      const { password, ...result } = user
      return result
    }
    return null
  }

  async generateJWT (user: any): Promise<AccessToken> {
    const payload: TokenPayload = {
      changePasswordAt: user.lastUpdatePassword,
      userID: user._id,
    }

    const accessToken = await jsonwebtoken.sign(
      payload,
      this.configService.jwtSecret,
      { expiresIn: '30d'},
    )

    return { accessToken }
  }
}