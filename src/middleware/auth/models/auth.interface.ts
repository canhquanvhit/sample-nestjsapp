export interface TokenPayload {
  userID: string
  changePasswordAt: number
}

export interface AccessToken {
  accessToken: string
}