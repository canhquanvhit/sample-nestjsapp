import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common'
import { roles } from 'src/constants/roles'

@Injectable()
export class Scopes implements CanActivate {
  constructor (
    private readonly requiredScopes: string[],
  ) { }

  canActivate(context: ExecutionContext): boolean {
    const requiredScopes = this.requiredScopes

    let userScopes = []
    const req = context.switchToHttp().getRequest()
    const user = req.user

    if (user.role.roleName === 'ADMIN') {
      return true
    }

    if (user.role.roleName === 'CLIENT') {
      userScopes = roles.user
    }

    for (const scope of requiredScopes) {
      if (!userScopes.includes(scope)) {
        return false
      }
    }

    return true
  }
}
