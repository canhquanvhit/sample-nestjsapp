interface ScopeRoles {
  clientScopes: string[],
  roles: string[],
}


// export const getRolesScope = (userAccess): ScopeRoles => {
//   const clientScopes = []

//   const { scopes }

// }

export const generateObjectId = (): string => {
  // tslint:disable-next-line: no-bitwise
  const timestamp = (new Date().getTime() / 1000 | 0).toString(16)

  const id = timestamp + 'xxxxxxxxxxxxxxxx'.replace(/[x]/g, () => {
      // tslint:disable-next-line:no-bitwise
      return (Math.random() * 16 | 0).toString(16)
  }).toLowerCase()

  return id
}